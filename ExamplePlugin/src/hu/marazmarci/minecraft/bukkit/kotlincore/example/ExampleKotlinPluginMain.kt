package hu.marazmarci.minecraft.bukkit.kotlincore.example

import hu.marazmarci.minecraft.bukkit.kotlincore.KotlinPluginModule
import hu.marazmarci.minecraft.bukkit.kotlincore.KotlinPluginContext
import hu.marazmarci.minecraft.bukkit.kotlincore.KotlinPlugin
import hu.marazmarci.minecraft.bukkit.kotlincore.extension.forEachPlayer
import hu.marazmarci.minecraft.bukkit.kotlincore.scheduleSyncDelayedTask
import org.bukkit.entity.Player


class MyTestKtPlugin : KotlinPlugin<MyTestKtPlugin>() {

    override fun createKotlinModule() = MyTestKtPluginModule()

    override val module: MyTestKtPluginModule
        get() = super.module as MyTestKtPluginModule

    override fun onLoadKt() {
        println("I'm a loaded plugin now! :)")
    }

    override fun onEnableKt() {
        println("I'm an enabled plugin now! :D")
    }

    override fun onDisableKt() {
        println("I'm an disabled plugin now! :(")
    }

    fun myOwnFunction() {
        println("asdlol")
    }

}



class MyTestKtPluginModule : KotlinPluginModule<MyTestKtPlugin>() {

    private val players = listOf<Player>()

    private val ktContext = KtContextTest(plugin)

    init {
        asd()
        scheduleSyncDelayedTask(99) {
            players.forEachPlayer {
                player.sendMessage("loool")
            }
        }
    }

    fun asd() {
        players.forEach(this::testPlayerMeta)
    }

    private fun testPlayerMeta(player: Player) {
        player.meta[5] = "asdasd"
        val readMeta = player.meta<String>()[5]
        println(readMeta)
        plugin.myOwnFunction()
        ktContext.asdlol(player)
    }


    override fun onDisableKt() {
        players.forEach { player ->
            player.sendMessage("Bye! :'(")
        }
    }

}


class KtContextTest(override val plugin: MyTestKtPlugin) : KotlinPluginContext<MyTestKtPlugin> {

    fun asdlol(player: Player) {
        player.meta[10] = 4.2
        val readMeta = player.meta<Double>()[10]
        println(readMeta)
        plugin.myOwnFunction()
    }

}