package hu.marazmarci.minecraft.bukkit.kotlincore

import org.bukkit.plugin.java.JavaPlugin


/**
 * A classic plugin base class like JavaPlugin.
 */
abstract class KotlinPlugin<T : KotlinPlugin<T>>(
    private val initModulesAfterMain: Boolean = true,
    internal val showWarningOnCommandOverride: Boolean = true,
    private val debugMode: Boolean = false // TODO
) : JavaPlugin(), KotlinPluginContext<T> {

    @Suppress("UNCHECKED_CAST")
    final override val plugin
        get() = this as T

    var modules: List<KotlinPluginModule<T>>? = null
        internal set

    open val module: KotlinPluginModule<T>?
        get() = modules?.run {
            if (size == 1)
                first()
            else
                null
        }

    open fun onLoadKt() {}
    open fun onEnableKt() {}
    open fun onDisableKt() {}

    final override fun onLoad() = onLoadKt()

    final override fun onEnable() {
        if (initModulesAfterMain) {
            onEnableKt()
            initKotlinModule()
        } else {
            initKotlinModule()
            onEnableKt()
        }
    }

    final override fun onDisable() {
        // reverse order compared to onEnable // TODO check if it's correct
        if (initModulesAfterMain) {
            disableKotlinModules()
            onDisableKt()
        } else {
            onDisableKt()
            disableKotlinModules()
        }
    }

    /**
     * Instantiate the modules.
     *
     * It calls createKotlinModules() first.
     * If null, it calls createKotlinModule().
     * The result will be stored in the `modules` property.
     */
    private fun initKotlinModule() {
        modules = createKotlinModules() ?: createKotlinModule()?.let { singleModule ->
            listOf(singleModule)
        }
    }

    private fun disableKotlinModules() {
        modules?.reversed()?.forEach {
            it.onDisableKt()
        }
        modules = null
    }


    open fun createKotlinModules(): List<KotlinPluginModule<T>>? = null
    open fun createKotlinModule(): KotlinPluginModule<T>? = null


    companion object {
        // TODO map of KotlinPlugins // TODO why??????????
    }

}


