package hu.marazmarci.minecraft.bukkit.kotlincore

import hu.marazmarci.minecraft.bukkit.kotlincore.extension.KtMetadatable
import org.bukkit.entity.Player
import org.bukkit.metadata.Metadatable
import java.util.logging.Logger

// TODO generic parameter: plugin type ?
interface KotlinPluginContext<T : KotlinPlugin<T>> {

    val plugin: T

    val Metadatable.meta
        get() = KtMetadatable<Any>(this, plugin)

    fun <T>Metadatable.meta()
            = KtMetadatable<T>(this, plugin)

    // TODO new extension property: lazyMeta ?

    companion object {
        val TICKS_PER_SECOND = 20
        val TICKS_PER_SECONDf = 20f
    }

}



val <T : KotlinPlugin<T>> KotlinPluginContext<T>.server
    get() = plugin.server!!

val <T : KotlinPlugin<T>> KotlinPluginContext<T>.log
    get() = KtLogger(plugin.logger)

inline class KtLogger(private val log: Logger) {
    fun s(msg: String) = log.severe(msg)
    fun w(msg: String) = log.warning(msg)
    fun i(msg: String) = log.info(msg)
    fun c(msg: String) = log.config(msg)
    fun f(msg: String) = log.fine(msg)
}


fun <T : KotlinPlugin<T>> KotlinPluginContext<T>.scheduleSyncDelayedTask(delayTicks: Long, task: () -> Unit)
        = server.scheduler.scheduleSyncDelayedTask(plugin, task, delayTicks)

fun <T : KotlinPlugin<T>> KotlinPluginContext<T>.scheduleSyncRepeatingTask(periodTicks: Long, delayTicks: Long = 0, task: () -> Unit)
        = server.scheduler.scheduleSyncRepeatingTask(plugin, task, delayTicks, periodTicks)

fun <T : KotlinPlugin<T>> KotlinPluginContext<T>.cancelScheduledTask(taskID: Int) = server.scheduler.cancelTask(taskID)



