package hu.marazmarci.minecraft.bukkit.kotlincore


/**
 * A rethink of the original plugin concept.
 * onEnable is constructor invocation :)
 * You can simply init your variables at the definition
 * and you can subscribe to events in the init {...} block
 *
 */
abstract class KotlinPluginModule<T : KotlinPlugin<T>> : KotlinPluginContext<T> {

    override lateinit var plugin: T
        internal set

    open fun onDisableKt() { }

}