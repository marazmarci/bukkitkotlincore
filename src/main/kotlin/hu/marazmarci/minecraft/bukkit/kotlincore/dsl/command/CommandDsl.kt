package hu.marazmarci.minecraft.bukkit.kotlincore.dsl.command

import hu.marazmarci.minecraft.bukkit.kotlincore.KotlinPlugin
import hu.marazmarci.minecraft.bukkit.kotlincore.KotlinPluginContext
import hu.marazmarci.minecraft.bukkit.kotlincore.log
import org.bukkit.command.*
import org.bukkit.entity.Player
import java.io.BufferedReader
import java.io.CharArrayReader
import java.io.CharArrayWriter
import java.io.PrintWriter


fun <T : KotlinPlugin<T>> KotlinPluginContext<T>.command(name: String, lambda: CommandDslRoot.() -> Unit) {

    val cmd = plugin.getCommand(name)

    if (cmd.executor != null && plugin.showWarningOnCommandOverride) { // command is already registered by the current plugin
        // show warning, then override previous registration
        plugin.log.w("Command ${cmd.name} was already registered, overriding previously registered executor.")
        plugin.log.w("Stack trace: ")
        val write = CharArrayWriter()
        Throwable().printStackTrace(PrintWriter(write))
        val read = BufferedReader(CharArrayReader(write.toCharArray()))
        read.lines().forEach(plugin.log::w)

    }

    CommandDslExecutor(lambda).let {
        cmd.executor = it
        cmd.tabCompleter = it
    }

}


class CommandDslExecutor(lambda: CommandDslRoot.() -> Unit) : CommandExecutor, TabCompleter {

    override fun onCommand(sender: CommandSender, cmd: Command, label: String, args: Array<String>): Boolean {
        val dsl = CommandDslRoot(sender)
        TODO("not implemented")
    }

    override fun onTabComplete(sender: CommandSender, cmd: Command, alias: String, args: Array<String>): MutableList<String> {
        TODO("not implemented")
    }

}


@CommandDslMarker
// prev name: Command_Dsl
abstract class CommandDslBase<SenderT : CommandSender>(private var senderBackingField: SenderT) {

    internal var sender: SenderT
        get() = senderBackingField
        internal inline set(value) {
            senderBackingField = value
        }

    internal val children = mutableListOf<CommandDslBase<*>>()

    internal fun tryRunCmd() {
        cmdRun()
        children.forEach(CommandDslBase<*>::tryRunCmd)
    }

    internal fun tryTabComplete() {
        tabComplete()
        children.forEach(CommandDslBase<*>::tryTabComplete)
    }

    protected abstract fun cmdRun()
    protected abstract fun tabComplete()

}


// prev name: Not_Exec
abstract class CommandDsl<SenderT : CommandSender>(sender: SenderT) : CommandDslBase<SenderT>(sender) {

    override fun cmdRun() = TODO("not implemented")
    override fun tabComplete() = TODO("not implemented")

    @CommandDslMarker
    fun exec(lambda: CommandDslExecBlock<SenderT>.() -> Unit) {
        children += CommandDslExecBlock(sender, lambda)
    }

    @CommandDslMarker
    fun subCommand(name: String, lambda: CommandDslSubCommand<SenderT>.() -> Unit) {
        TODO()
    }

    @CommandDslMarker
    fun require(cond: Boolean, lambda: CommandDsl<SenderT>.() -> Unit) {
        TODO()
    }

    @CommandDslMarker
    fun requirePlayer(lambda: CommandDslRequirePlayer.(player: Player) -> Unit) {

        val player = sender as? Player

        if (player != null) {
            children += CommandDslRequirePlayer(player)
        } else {
            // TODO result: not applicable
        }
    }

    @CommandDslMarker
    fun argPlayer(name: String = "player", lambda: CommandDsl<SenderT>.(player: Player) -> Unit) {
        TODO("not implemented")
    }

    @CommandDslMarker
    fun argInt(name: String, lambda: (int: Int) -> Unit) {
        TODO("not implemented")
    }
}

class CommandDslSubCommand<SenderT : CommandSender>(sender: SenderT) : CommandDsl<SenderT>(sender) {

}


class CommandDslRoot(sender: CommandSender) : CommandDsl<CommandSender>(sender) {

    override fun cmdRun() = TODO("not implemented")
    override fun tabComplete() = TODO("not implemented")

}



class CommandDslExecBlock<SenderT : CommandSender>(
    sender: SenderT, private val lambda: CommandDslExecBlock<SenderT>.() -> Unit
) : CommandDslBase<SenderT>(sender) {

    override fun cmdRun() = TODO("not implemented")
    override fun tabComplete() = TODO("not implemented")

    // TODO run exec block lambda

}








abstract class Require<SenderT : CommandSender>(sender: SenderT) : CommandDsl<SenderT>(sender) {

    override fun cmdRun() = TODO("not implemented")
    override fun tabComplete() = TODO("not implemented")

}

class CommandDslRequirePlayer(sender: Player) : Require<Player>(sender) {

    override fun cmdRun() = TODO("not implemented")
    override fun tabComplete() = TODO("not implemented")

    val senderPlayer: Player = sender

}

/*class CommandDslConsoleSender : CommandDslBase<ConsoleCommandSender>() {
    val console: ConsoleCommandSender = sender
}

class CommandDslBlockSender : CommandDslBase<BlockCommandSender>() {
    val commandBlock = sender
}*/


