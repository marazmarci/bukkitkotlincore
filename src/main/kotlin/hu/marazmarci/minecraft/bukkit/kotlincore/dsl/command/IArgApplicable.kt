package hu.marazmarci.minecraft.bukkit.kotlincore.dsl.command

import org.bukkit.command.CommandSender
import org.bukkit.entity.Player


private interface IArgApplicable <SenderT : CommandSender> {

    @CommandDslMarker
    fun argPlayer(argName: String = "player", lambda: CommandDsl<SenderT>.(player: Player) -> Unit)

    @CommandDslMarker
    fun argInt(argName: String, lambda: CommandDsl<SenderT>.(int: Int) -> Unit)

}