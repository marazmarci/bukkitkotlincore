package hu.marazmarci.minecraft.bukkit.kotlincore.dsl.command

import org.bukkit.command.CommandSender


private interface IKnownSender <SenderT : CommandSender> {
    var sender: SenderT
}