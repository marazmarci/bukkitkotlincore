package hu.marazmarci.minecraft.bukkit.kotlincore.dsl.command

import hu.marazmarci.minecraft.bukkit.kotlincore.KotlinCore
import hu.marazmarci.minecraft.bukkit.kotlincore.KotlinPluginContext
import hu.marazmarci.minecraft.bukkit.kotlincore.extension.copy


private object Test : KotlinPluginContext<KotlinCore> {

    override val plugin
        get() = KotlinCore.instance


    fun asd() {

        command("tpMeTo") {
            requirePlayer { me ->
                val b = this
                argPlayer { otherPlayer ->
                    val c = this
                    exec {
                        me.teleport(otherPlayer.location)
                    }
                }
                argInt("x") { x ->
                    argInt("y") { y ->
                        argInt("z") { z ->
                            me.teleport(me.location.copy(x, y, z))
                        }
                    }
                }
            }
        }

        command("test") {
            requirePlayer {
                subCommand("lol") {
                    val a = sender
                }
                subCommand("wow") {

                }
            }
            subCommand("asd") {

            }
        }

    }


}