package hu.marazmarci.minecraft.bukkit.kotlincore.extension

import org.bukkit.block.Block

operator fun Block.component1() = x
operator fun Block.component2() = y
operator fun Block.component3() = z


/**
 * @throws IllegalArgumentException if the locations are in different worlds
 */
operator fun Block.rangeTo(other: Block): ClosedRange<KtBlock> {
    if (this.world != other.world)
        throw IllegalArgumentException("the blocks are in different worlds!")
    return BlockRange(this.toKt(), other.toKt())
}


data class KtBlock(private val block: Block) : Block by block, Comparable<KtBlock> {

    override fun compareTo(other: KtBlock): Int {
        TODO("not implemented")
    }

}

fun Block.toKt() = KtBlock(this)


class BlockRange(override val endInclusive: KtBlock, override val start: KtBlock) : ClosedRange<KtBlock> {

    // TODO test
    override fun contains(value: KtBlock): Boolean {
        if (value.x !in start.x..endInclusive.x) return false
        if (value.y !in start.y..endInclusive.y) return false
        if (value.z !in start.z..endInclusive.z) return false
        return true
    }

    // TODO test
    override fun isEmpty() = start == endInclusive
}