package hu.marazmarci.minecraft.bukkit.kotlincore.extension

import org.bukkit.Chunk
import org.bukkit.ChunkSnapshot
import kotlin.concurrent.thread

fun Chunk.processSnapshotAsync(action: (ChunkSnapshot) -> Unit, includeMaxblocky: Boolean = false, includeBiome: Boolean = false, includeBiomeTempRain: Boolean = false) {
    val snapshot = this.getChunkSnapshot(includeMaxblocky, includeBiome, includeBiomeTempRain)!!
    thread(start = true) {
        action.invoke(snapshot)
        // TODO give option to handle onResult in main thread
    }
}