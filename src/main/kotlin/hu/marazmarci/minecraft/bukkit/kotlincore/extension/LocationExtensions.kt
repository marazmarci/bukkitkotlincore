package hu.marazmarci.minecraft.bukkit.kotlincore.extension

import org.bukkit.Location
import org.bukkit.World
import org.bukkit.block.BlockFace
import org.bukkit.util.Vector

operator fun Location.component1() = x
operator fun Location.component2() = y
operator fun Location.component3() = z
operator fun Location.component4() = pitch
operator fun Location.component5() = yaw

operator fun Location.plus(vec: Vector) = copy().apply { this += vec }
operator fun Location.plusAssign(vec: Vector) { add(vec) }
operator fun Location.minus(vec: Vector) = copy().apply { this -= vec }
operator fun Location.minusAssign(vec: Vector) { subtract(vec) }

operator fun Location.plus(loc: Location) = copy().apply { this += loc }
operator fun Location.plusAssign(loc: Location) { add(loc) }
operator fun Location.minus(loc: Location) = copy().apply { this -= loc }
operator fun Location.minusAssign(loc: Location) { subtract(loc) }

operator fun Location.plus(blockFace: BlockFace) = this + blockFace.direction
operator fun Location.plusAssign(blockFace: BlockFace) { this += blockFace.direction }
operator fun Location.minus(blockFace: BlockFace) = this - blockFace.direction
operator fun Location.minusAssign(blockFace: BlockFace) { this -= blockFace.direction }

operator fun Location.times(scalar: Double) = copy().apply { this *= scalar }
operator fun Location.timesAssign(scalar: Double) { multiply(scalar) }
operator fun Location.times(scalar: Number) = this * scalar.toDouble()
operator fun Location.timesAssign(scalar: Number) { this *= scalar.toDouble() }
operator fun Location.div(scalar: Double) = copy().apply { this /= scalar }
operator fun Location.divAssign(scalar: Double) { divide(scalar) }
operator fun Location.div(scalar: Number) = this / scalar.toDouble()
operator fun Location.divAssign(scalar: Number) { this /= scalar.toDouble() }


fun Location.multiply(scalar: Number) = multiply(scalar.toDouble())!!
fun Location.divide(scalar: Double) = multiply(1.0 / scalar)!!
fun Location.divide(scalar: Number) = divide(scalar.toDouble())


fun Location.copy(x: Number = this.x, y: Number = this.y, z: Number = this.z, yaw: Number = this.yaw, pitch: Number = this.pitch, world: World = this.world)
        = Location(world, x.toDouble(), y.toDouble(), z.toDouble(), yaw.toFloat(), pitch.toFloat())

/**
 * @throws IllegalArgumentException if the locations are in different worlds
 */
operator fun Location.rangeTo(other: Location): KtLocationIterator = this.toKt()..other.toKt()


/**
 * @throws IllegalArgumentException if the locations are in different worlds
 */
operator fun KtLocation.rangeTo(other: KtLocation): KtLocationIterator {
    if (this.world != other.world)
        throw IllegalArgumentException("the locations are in different worlds!")
    return KtLocationIterator(this, other)
}


data class KtLocation(private val l: Location) : Location(l.world, l.x, l.y, l.z, l.yaw, l.pitch), Comparable<KtLocation> {

    override fun compareTo(other: KtLocation): Int {
        TODO("not implemented")
    }

}

fun Location.toKt() = KtLocation(this)


class KtLocationIterator(override val start: KtLocation, override val endInclusive: KtLocation) : ClosedRange<KtLocation>, Iterable<KtLocation>, Iterator<KtLocation> {

    private var x = start.x
    private var y = start.y
    private var z = start.z
    private var remainingCount = (endInclusive.x - x + 1) * (endInclusive.y - y + 1) * (endInclusive.z - z + 1)

    override fun hasNext() = remainingCount > 0

    override fun next(): KtLocation {
        TODO("not implemented")
        // TODO decrement remainingCount
    }

    override fun iterator() = this

    // TODO test
    override fun contains(value: KtLocation): Boolean {
        if (value.x !in start.x..endInclusive.x) return false
        if (value.y !in start.y..endInclusive.y) return false
        if (value.z !in start.z..endInclusive.z) return false
        return true
    }

    // TODO test
    override fun isEmpty() = start == endInclusive

    fun forEachCoordinate(action: (x: Int, y: Int, z: Int) -> Unit) {
        TODO("not implemented")
    }

}