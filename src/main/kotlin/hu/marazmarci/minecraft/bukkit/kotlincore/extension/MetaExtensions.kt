package hu.marazmarci.minecraft.bukkit.kotlincore.extension

import org.bukkit.metadata.FixedMetadataValue
import org.bukkit.metadata.Metadatable
import org.bukkit.plugin.Plugin

// TODO examples
class KtMetadatable<T>(
    private val metadatable: Metadatable, private val plugin: Plugin
) : Metadatable by metadatable {

    /**
     * Returns all the metadata which is assigned to this metadatable by the current plugin
     */
    operator fun get(key: String): List<T?> = getMetadata(key)
        .filter { it.owningPlugin == plugin }
        .map { it.value() as? T }
    operator fun get(key: Number): List<T?> = this[key.toString()]

    /**
     *
     */
    operator fun set(key: String, value: T?) = setMetadata(key, FixedMetadataValue(plugin, value))
    operator fun set(key: Number, value: T?) { this[key.toString()] = value }

}