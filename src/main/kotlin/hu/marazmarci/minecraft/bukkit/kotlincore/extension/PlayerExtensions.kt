package hu.marazmarci.minecraft.bukkit.kotlincore.extension

import org.bukkit.entity.Player


inline class PlayerWrapper(val player: Player)

fun Iterable<Player>.forEachPlayer(block: PlayerWrapper.() -> Unit) = forEach {
    PlayerWrapper(it).block()
}