package hu.marazmarci.minecraft.bukkit.kotlincore.extension

fun String.concatIf(condition: Boolean, otherStr: String)
        = if (condition) this + otherStr else this