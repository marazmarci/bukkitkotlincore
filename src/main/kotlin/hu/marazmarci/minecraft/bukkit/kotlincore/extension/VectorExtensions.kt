package hu.marazmarci.minecraft.bukkit.kotlincore.extension

import org.bukkit.util.Vector

operator fun Vector.component1() = x
operator fun Vector.component2() = y
operator fun Vector.component3() = z

operator fun Vector.plus(other: Vector) = copy().apply { this += other }
operator fun Vector.plusAssign(other: Vector) { add(other) }
operator fun Vector.minus(other: Vector) = copy().apply { this -= other }
operator fun Vector.minusAssign(other: Vector) { subtract(other) }
operator fun Vector.div(scalar: Double) = copy().apply { this /= scalar }
operator fun Vector.div(scalar: Number) = this / scalar.toDouble()
operator fun Vector.divAssign(scalar: Double) { divide(scalar) }
operator fun Vector.divAssign(scalar: Number) { this /= scalar.toDouble() }
operator fun Vector.times(scalar: Double) = copy().apply { this *= scalar }
operator fun Vector.times(scalar: Number) = this * scalar.toDouble()
operator fun Vector.timesAssign(scalar: Double) { multiply(scalar) }
operator fun Vector.timesAssign(scalar: Number) { this *= scalar.toDouble() }

operator fun Vector.unaryMinus() = copy().multiply(-1.0)!!

infix fun Vector.cross(other: Vector) = getCrossProduct(other)!!
infix fun Vector.dot(other: Vector) = dot(other)

fun Vector.divide(scalar: Double) = multiply(1.0 / scalar)!!
fun Vector.divide(scalar: Number) { divide(scalar.toDouble()) }

fun Vector.copy(x: Double = this.x, y: Double = this.y, z: Double = this.z) = Vector(x,y,z)